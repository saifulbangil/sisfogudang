<?php if ($this->session->flashdata('success')): ?>
<div class="alert alert-success" role="alert">
	<?php echo $this->session->flashdata('success'); ?>
</div>
<?php endif; ?>
<?php if ($this->session->flashdata('error')): ?>
<div class="alert alert-danger" role="alert">
	<?php echo $this->session->flashdata('error'); ?>
</div>
<?php endif; ?>

<div class="card mb-3">
    <div class="card-body">
        <form method="post" action="<?php echo site_url('skmasuk/approve/').$sk_masuk[0]['id_sk_masuk']; ?> ">
            <div class="form-group">
            <label for="name">Tipe Mobil*</label>
                <input class="form-control" type="text" name="tipe_mobil" value="<?php echo $sk_masuk[0]['tipe_mobil']; ?>" readonly/>
            </div>
            <div class="form-group">
              <label for="name">Warna Mobil*</label>
                <input class="form-control" type="text" name="warna_mobil" value="<?php echo $sk_masuk[0]['warna_mobil']; ?>" readonly/>
            </div>
            <div class="form-group">
              <label for="name">No. Rangka*</label>
                <input class="form-control" type="text" name="nomor_rangka" value="<?php echo $sk_masuk[0]['nomor_rangka']; ?>" readonly/>
            </div>
            <div class="form-group">
              <label for="name">Mobil Dari*</label>
              <input class="form-control" type="text" name="asal_mobil" value="<?php echo $sk_masuk[0]['asal_mobil']; ?>" readonly/>
            </div>
            <div class="form-group">
                <label for="name">Tanggal*</label>
                <input class="form-control" type="text" name="tanggal" value="<?php echo $sk_masuk[0]['tanggal']; ?>" readonly/>
            </div>
            <div class="form-group">
                <label for="name">Pembawa (Driver)*</label>
                <input class="form-control" type="text" name="pembawa" value="<?php echo $pembawa[0]['name']; ?>" readonly/>
            </div>
            <div class="form-group">
              <label for="name">Jam Tiba*</label>
                <input class="form-control" type="text" name="jam_tiba" value="<?php echo $sk_masuk[0]['jam_tiba']; ?>" readonly/>
            </div>
            <div class="form-group">
              <label for="name">Keterangan</label>
                <input class="form-control" type="text" name="keterangan" id="keterangan" value="<?php echo $sk_masuk[0]['keterangan']; ?>" <?php echo ($this->session->userdata['_type'] !== 'admin') ? 'readonly' : ''; ?>/>
            </div>
            <div class="form-group">
              <label for="name">Dibuat Oleh ;</label>
                <?php echo $dibuat_oleh[0]['name']; ?>
            </div>
            <?php
              if ($sk_masuk[0]['status'] !== 'APPROVED' && $sk_masuk[0]['status'] !== 'DITOLAK') {
                if ($this->session->userdata['_type'] === 'admin') {
                  ?>
                    <input class="btn btn-success" type="submit" value="BUAT & KIRIM SK">
                    <a href="#" onClick="tolakSK()" class="btn btn-danger" style="float: right;">TOLAK PENGAJUAN SK</a>
                  <?php
                } else if ($this->session->userdata['_type'] === 'pic') {
                  ?>
                    <input class="btn btn-success" type="submit" value="APPROVE SK">
                  <?php
                }
              }
            ?>
        </form>
    </div>
</div>

<div class="card-footer small text-muted">
	* Harus Diisi
</div>

<script>
    function tolakSK() {
      var keterangan = document.getElementById('keterangan').value;
      var url = '<?php echo site_url('skmasuk/tolaksk/').$sk_masuk[0]['id_sk_masuk'];?>/'+keterangan;
      window.location.href = url;
    }
</script>