<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
      <div class="sidebar-brand-text mx-3">Admin</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Data Stok -->
      <li class="nav-item <?php echo $this->uri->segment(2) == 'datastok' || $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('datastok') ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Tambah Data Stok</span></a>
      </li>

      <li class="nav-item <?php echo $this->uri->segment(2) == 'datastok' || $this->uri->segment(2) == 'list' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('datastok/list') ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Data Stok</span></a>
      </li>
      
      <!-- Nav Item - Approval SK-Masuk -->
      <li class="nav-item <?php echo $this->uri->segment(2) == 'skmasuk' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('approval/skmasuk') ?>">
          <i class="fas fa-fw fa-envelope"></i>
          <span>Daftar Approval Sk-Masuk</span></a>
      </li>
      
      <!-- Nav Item - Approval SK-Keluar-->
      <li class="nav-item <?php echo $this->uri->segment(2) == 'skkeluar' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('approval/skkeluar') ?>">
          <i class="fas fa-fw fa-envelope"></i>
          <span>Approval Sk-Keluar</span></a>
      </li>

      <!-- Nav Item - Penugasan -->
      <!-- <li class="nav-item <?php echo $this->uri->segment(1) == 'skkeluar' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('skkeluar') ?>">
          <i class="fas fa-fw fa-envelope"></i>
          <span>Buat SK-Keluar</span></a>
      </li> -->
      
      <!-- Nav Item - Laporan SK Masuk -->
      <!-- <li class="nav-item <?php echo $this->uri->segment(1) == 'laporanskmasuk' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('laporanskmasuk') ?>">
          <i class="fas fa-fw fa-envelope"></i>
          <span>Laporan SK-Masuk</span></a>
      </li> -->

      <!-- Nav Item - Laporan SK Keluar -->
      <!-- <li class="nav-item <?php echo $this->uri->segment(1) == 'laporanskkeluar' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('laporanskkeluar') ?>">
          <i class="fas fa-fw fa-envelope"></i>
          <span>Laporan SK-Keluar</span></a>
      </li> -->

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
