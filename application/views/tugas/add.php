<?php if ($this->session->flashdata('success')): ?>
<div class="alert alert-success" role="alert">
	<?php echo $this->session->flashdata('success'); ?>
</div>
<?php endif; ?>
<?php if ($this->session->flashdata('error')): ?>
<div class="alert alert-error" role="alert">
	<?php echo $this->session->flashdata('error'); ?>
</div>
<?php endif; ?>

<div class="card mb-3">
    <div class="card-body">
        <form method="post" action="<?php echo site_url('tugas/buattugas')?>">
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="name">Tipe Mobil*</label>
                    <select name="tipe_mobil" id="tipe_mobil" class="form-control" onchange="getWarnaMobil()" required>
                        <option value="" disabled selected>PILIH TIPE MOBIL</option>
                        <?php foreach ($list_tipe_mobil->result() as $data): ?>
                            <option value="<?php echo $data->tipe_mobil; ?>"><?php echo $data->tipe_mobil; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="name">Warna*</label>
                    <select name="warna_mobil" id="warna_mobil" class="form-control"required>
                        <option value="" disabled selected>PILIH WARNA MOBIL</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="name">Peruntukan*</label>
                    <select name="peruntukan" id="peruntukan" class="form-control" onchange="setStatusJamKembali(this.value)" required>
                        <option value="" disabled selected>PILIH PERUNTUKAN</option>
                        <option value="dipinjam">DIPINJAM</option>
                        <option value="dipinjam sehari">DIPINJAM SEHARI</option>
                        <option value="dibeli">DIBELI</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="name">Permintaan Dari*</label>
                    <select name="permintaan_dari" class="form-control">
                        <option value="Main Dealer">Main Dealer</option>
                        <option value="Dealer">Dealer</option>
                        <option value="Cabang Lain">Cabang Lain</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-4">
                    <label for="name">Tanggal*</label>
                    <input type="date" class="form-control" required name="tanggal"/>
                </div>
                <div class="col-md-4">
                    <label for="name">Jam Keluar*</label>
                    <input type='time' class="form-control" id="jam_keluar" onfocusout="cekWaktu()" required name="jam_keluar"/>
                </div>
                <div class="col-md-4">
                    <label for="name">Jam Kembali</label>
                    <input type='time' class="form-control" id="jam_kembali" onfocusout="cekWaktu()" name="jam_kembali"/>
                </div>
            </div>
            <input class="btn btn-success" type="submit" value="AJUKAN PERMINTAAN">
        </form>
    </div>
</div>
<div class="card mb-3">
        <div class="col-md-12" style="text-align: center; margin-top: 1%; font-size: x-large;">
            DATA STOK YANG TERSEDIA
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>NO</th>
							<th>Type Mobil</th>
							<th>Warna Mobil</th>
							<th>Nomor Rangka</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no = 1; 
							foreach ($list->result() as $dt): ?>
						<tr>
							<td>
								<?php echo $no++; ?>
							</td>
							<td>
								<?php echo $dt->tipe_mobil; ?>
							</td>
							<td>
								<?php echo $dt->warna_mobil; ?>
				             </td>
							<td>
								<?php echo $dt->nomor_rangka; ?>
            				</td>	
						</tr>
						<?php endforeach; ?>
					</tbody>
        </table>
			</div>
		</div>
	</div>


<div class="card-footer small text-muted">
	* Harus Diisi
</div>

<script>
    function getWarnaMobil() {
        var tipe_mobil = document.getElementById("tipe_mobil").value;
        var warna_mobil = $('#warna_mobil');
        $.ajax({
            type: "GET",
            url: '<?php echo site_url('tugas/warnaMobil')?>/'+tipe_mobil,
            success: function(data){
                warna_mobil.html(data);                
            }
        });
    }
    function cekWaktu() {
        var jam_keluar = document.getElementById('jam_keluar').value;
        var jam_kembali = document.getElementById('jam_kembali').value;
        if (jam_kembali !== '') {
            if (jam_kembali < jam_keluar) {
                alert('Jam Kembali tidak boleh kurang dari Jam Keluar');
                document.getElementById('jam_keluar').value = '';
                document.getElementById('jam_kembali').value = '';
            }
        }
    }
    function setStatusJamKembali(peruntukan) {
        if (peruntukan === 'dipinjam sehari') {
            document.getElementById('jam_kembali').readOnly = false;
        } else {
            document.getElementById('jam_kembali').readOnly = true;
        }
    }
</script>