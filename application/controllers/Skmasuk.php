<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skmasuk extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->login_model->logged_id()){
			redirect('Auth','refresh');
		}
	}
	
	public function index()
	{
		$data['page'] = 'skmasuk/add';
		$data['sidebar'] = $this->session->userdata['_type'];
		$sql = "SELECT tipe_mobil FROM data_stok GROUP BY tipe_mobil ORDER BY tipe_mobil ASC";
		$data['list_tipe_mobil'] = $this->db->query($sql);
		
		$data['list_driver'] = $this->getDriver();
		$this->load->view('_partials/template', $data);
	}
	public function detail($id_sk_masuk)
	{
		$data['page'] = 'skmasuk/detail';
		$data['sidebar'] = $this->session->userdata['_type'];
		$sql = "SELECT * FROM sk_masuk WHERE id_sk_masuk = $id_sk_masuk";
		$data['sk_masuk'] = $this->db->query($sql)->result_array();;
		$sql = "SELECT name FROM users WHERE id = ".$data['sk_masuk'][0]['id_security'];
		$data['dibuat_oleh'] = $this->db->query($sql)->result_array();
		$sql = "SELECT name FROM users WHERE id = ".$data['sk_masuk'][0]['pembawa'];
		$data['pembawa'] = $this->db->query($sql)->result_array();

		
		$this->load->view('_partials/template', $data);
	}
	public function buatskmasuk() {
		if (isset($_POST)) {
			$var = $this->session->userdata;

			$_POST['id_security'] = $var['_user_id'];
			if ($_POST['jam_tiba'] == '00:00:00') {
				$_POST['jam_tiba'] = null;
			}
			$add = $this->db->insert('sk_masuk', $_POST);
			if($add) {
				$id = $this->db->insert_id();
				$getData = $this->db->get_where('users', array('id' => $id))->result_array();
				$notif['dari'] = $this->session->userdata['_name'];
				$notif['pesan'] = 'Ada pengajuan pembuatan SK Masuk dari '.$this->session->userdata['_name'];
				$notif['tipe'] = 'info';

				$this->db->where('type', 'admin');
				$getPic = $this->db->get('users');
				$body = str_replace(' ', '%20', $notif['pesan']);
				foreach ($getPic->result() as $pic):
					$notif['untuk'] = $pic->id;
					$this->db->insert('user_notifikasi', $notif);
					$email = str_replace('@','-at-', $pic->email);
					$urlEmail = site_url('email/index/'.$email.'/'.$body);
					fopen($urlEmail, "r");
				endforeach;

				$this->session->set_flashdata('success', "PENGAJUAN SK MASUK BERHASIL DIBUAT");
			} else {
				$this->session->set_flashdata('error', "GAGAL MEMBUAT PENGAJUAN SK MASUK");
			}
		}

		echo '<script type="text/javascript">
				    window.location.href="'.$_SERVER['HTTP_REFERER'].'";
				</script>';
	}
	public function approve($id_sk_masuk){
		if($this->session->userdata['_type'] === 'admin') {
			$data['status'] = 'MENUNGGU APPROVAL PIC';
			$data['keterangan'] = $_POST['keterangan'];
		} else if($this->session->userdata['_type'] === 'pic') {
			$data['status'] = 'APPROVED';
		}
		$this->db->where('id_sk_masuk', $id_sk_masuk);
		$update = $this->db->update('sk_masuk', $data);
		if($update) {
			if($this->session->userdata['_type'] === 'pic') {
				$mobil['tipe_mobil'] = $_POST['tipe_mobil'];
				$mobil['warna_mobil'] = $_POST['warna_mobil'];
				$mobil['nomor_rangka'] = $_POST['nomor_rangka'];
				$mobil['stok'] = 1;
				$insert = $this->db->insert('data_stok', $mobil);
			} else {
				$insert = true;
			}
			if($insert) {
				$this->db->where('id_sk_masuk', $id_sk_masuk);
				$getData = $this->db->get('sk_masuk')->result_array();
				$notif['dari'] = $this->session->userdata['_name'];
				$notif['untuk'] = $getData[0]['id_security'];
				$notif['tipe'] = 'success';
				if($this->session->userdata['_type'] === 'admin') {
					$notif['pesan'] = 'Ada pengajuan SK Masuk dengan id '. $id_sk_masuk.'.';

					$this->db->where('type', 'pic');
					$getAdmin = $this->db->get('users');
					foreach ($getAdmin->result() as $pic):
						$notif['untuk'] = $pic->id;
						$this->db->insert('user_notifikasi', $notif);
					endforeach;
				} else if($this->session->userdata['_type'] === 'pic') {
					$notif['pesan'] = 'Pengajuan SK Masuk dengan id '. $id_sk_masuk.' telah di approve oleh PIC';
					$this->db->insert('user_notifikasi', $notif);
	
					$body = str_replace(' ', '%20', $notif['pesan']);
					$sql = "SELECT * FROM users WHERE id = ".$getData[0]['id_security'];
					$data['email'] = $this->db->query($sql)->result_array();
					$email = str_replace('@','-at-', $data['email'][0]['email']);
					$urlEmail = site_url('email/index/'.$email.'/'.$body);
					fopen($urlEmail, "r");
				}

				$this->session->set_flashdata('success', "SK BERHASIL DIAPPROVE");
			} else {
				$this->session->set_flashdata('error', "GAGAL MENGUBAH STOK MOBIL");
			}
		} else {
			$this->session->set_flashdata('error', "GAGAL MENGUBAH STATUS SK KELUAR");
		}

		echo '<script type="text/javascript">
				    window.location.href="'.$_SERVER['HTTP_REFERER'].'";
				</script>';
	}
	public function getDriver() {
		$sql = "SELECT * FROM users WHERE type = 'driver' ORDER BY name";
		$list_driver = $this->db->query($sql);
		$list_driver_available = [];
		foreach ($list_driver->result() as $driver):
			array_push($list_driver_available, $driver);
		endforeach;
		return $list_driver_available;
	}
	public function tolaksk($id_sk_masuk, $keterangan){
		$var = $this->session->userdata;
		$data['status'] = 'DITOLAK';
		$data['keterangan'] =  str_replace('%20',' ',$keterangan);
		$this->db->where('id_sk_masuk', $id_sk_masuk);
		$update = $this->db->update('sk_masuk', $data);
		if($update) {
			$this->db->where('id_sk_masuk', $id_sk_masuk);
			$getData = $this->db->get('sk_masuk')->result_array();
			$notif['dari'] = $this->session->userdata['_name'];
			$notif['untuk'] = $getData[0]['id_security'];
			$notif['pesan'] = 'Pengajuan SK Masuk dengan id '. $id_sk_masuk.' ditolak';
			$notif['tipe'] = 'danger';
			$this->db->insert('user_notifikasi', $notif);

			$body = str_replace(' ', '%20', $notif['pesan']);
			$sql = "SELECT * FROM users WHERE id = ".$getData[0]['id_security'];
			$data['email'] = $this->db->query($sql)->result_array();
			$email = str_replace('@','-at-', $data['email'][0]['email']);
			$urlEmail = site_url('email/index/'.$email.'/'.$body);
			fopen($urlEmail, "r");

			$this->session->set_flashdata('success', "STATUS PENGAJUAN SK BERHASIL DIUBAH");
		} else {
			$this->session->set_flashdata('error', "GAGAL MENGUBAH STATUS PENGAJUAN SK KELUAR");
		}

		echo '<script type="text/javascript">
				    window.location.href="'.$_SERVER['HTTP_REFERER'].'";
				</script>';
	}
	public function ceknomorrangka($nomor_rangka) {
		if ($nomor_rangka != '') {
			$sql = "SELECT * FROM data_stok WHERE nomor_rangka = '$nomor_rangka'";
			$result = $this->db->query($sql)->result_array();
			$status = (count($result) == 0 ? 'available' : 'not available');
			echo $status;
			return $status;
		}
	}
}