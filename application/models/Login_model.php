<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model
{
    private $tabelName = "users";

    //fungsi cek session
    public function logged_id()
    {
        return $this->session->userdata('_type');
    }

    //fungsi check login
    public function check_login($username, $password)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($username);
        $this->db->where($password);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }
}